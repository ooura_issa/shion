﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Vector3 Camerapos;
    public Vector3 LeftWall;
    public Vector3 RightWall;
    public Vector3 TopWall;
    public Vector3 BottomWall;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(gameObject.name);
    }

    public void Position()
    {
        Camerapos = GameObject.Find("Main Camera").transform.position;
        LeftWall = GameObject.Find("Leftwall").transform.position;
        RightWall = GameObject.Find("Rightwall").transform.position;
        TopWall = GameObject.Find("Topwall").transform.position;
        BottomWall = GameObject.Find("Bottomwall").transform.position;
    }
   
    // Update is called once per frame
    void Update()
    {
        
    }
}
