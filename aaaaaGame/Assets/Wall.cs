﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    private Vector3 Camerapos;
    private Vector3 vector3cam;
    private bool stop;
    //public GameObject Camera;
    // Start is called before the first frame update
    void Start()
    {
        stop = false;
        Camerapos= GameObject.Find("Main Camera").transform.position;
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        stop = true;
    }

    //private void OnTriggerExit2D(Collider2D collision)
    //{
    //    stop = false;
    //}
    
    public bool Stop()
    {
        return stop;
    }

    // Update is called once per frame
    void Update()
    {
        if(stop==true)
        {
            Debug.Log("a");
        }
    }
}
