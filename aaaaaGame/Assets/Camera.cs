﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    //private GameObject Camera
    private Vector3 Camerasize;
    private Vector3 a;
    private List<bool> walls = new List<bool>();
    
    // Start is called before the first frame update
    void Start()
    {
        Camerasize = GameObject.Find("Main Camera").transform.localScale;
        transform.localScale = Camerasize;
    }

    public void Controll()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            //Debug.Log(Cameracontlorl);
            a.x -= 1;
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            a.x += 1;
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            a.y += 1;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            a.y -= 1;
        }
        transform.position = a;
    }

    
    // Update is called once per frame
    void Update()
    {
        Controll();
    }
}
