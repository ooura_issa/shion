﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{
    private Vector3 Camera;
    private Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        

        offset = transform.position - Camera;
    }

    public void Position()
    {
        transform.position = Camera + offset;
    }

    // Update is called once per frame
    void Update()
    {
        Camera = GameObject.Find("Camera").transform.position;
        Position();
    }
}
