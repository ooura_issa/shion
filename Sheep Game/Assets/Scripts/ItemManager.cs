﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    private Vector3 itemScale;
    private List<GameObject> itemlist = new List<GameObject>();
    public GameObject trap;
    public GameObject wave;
    public GameObject meat;

    private int count = 0;

    // Start is called before the first frame update
    void Start()
    {
       
        itemlist.Add(trap);
        itemlist.Add(wave);
        itemlist.Add(meat);
        count = 0;
        itemScale = gameObject.transform.localScale; 
    }
   
    public void Generate()//ランダムに出現
    {
        count += 1;
        GameObject a;
        //Debug.Log(count);
        float transform = Random.Range(-7, 8);
        if(count==300)
        {  
            
            a=Instantiate(itemlist[Random.Range(0,3)], new Vector3(transform, 0), Quaternion.identity);
            
            count = 0;
        }
    }

    // Update is called once per frame
    void Update()
    {
    
        Generate();
        
    }
}
