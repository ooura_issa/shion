﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playerctr : MonoBehaviour
{
    float velocity;
    public Vector3 LeftWall;
    public Vector3 RightWall;
    public Vector3 BottomWall;
    public GameObject Icons;
    public GameObject IconChild;
    public GameObject dougu;
    public GameObject trapDG;
    public GameObject waveDG;
    public GameObject meatDG;
    // Start is called before the first frame update
    void Start()
    {
        IconChild = null;
        dougu = GameObject.Find("Dougu");
        LeftWall =GameObject.Find("leftwall"). transform.position;
        RightWall =GameObject.Find("rightwall"). transform.position;
        BottomWall = GameObject.Find("bottomwall").transform.position;
       
        velocity = -0.05f;
    }

    
    public GameObject ItemIcon(string name)//Iconsの子オブジェクトからnameで指定したオブジェクトを取得
    {
        foreach(Transform a in Icons.transform)
        {
            if(a.name==name)
            {
                IconChild = a.gameObject;
                
            }
        }
        return IconChild;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        List<GameObject> list=dougu.GetComponent<DouguManager>().aaaa();
        if (collision.tag == "wall")
        {
            velocity = 0;
            Debug.Log("ppp");
        }

        if (collision.name == "Meat(Clone)")
        {
            ItemIcon("Meat icon");
            
            if(list.Count>=3)
            {
                return;
            }
            else
            {
                dougu.GetComponent<DouguManager>().Dougutool(meatDG);
                collision.gameObject.SetActive(false);
            }
            IconChild.SetActive(true);
            
        }
        else if (collision.name == "Trap(Clone)")
        {
            ItemIcon("Trap icon");
            //dougu.GetComponent<DouguManager>().Dougutool(trapDG);
            if (list.Count >= 3)
            {
                return;
            }
            else
            {
                dougu.GetComponent<DouguManager>().Dougutool(trapDG);
                collision.gameObject.SetActive(false);
            }
            //collision.gameObject.SetActive(false);
            IconChild.SetActive(true);
            
        }
        else if (collision.name == "Wave(Clone)")
        {
            ItemIcon("Wave icon");
            if (list.Count >=3)
            {
                return;
            }
            else
            {
                dougu.GetComponent<DouguManager>().Dougutool(waveDG);
                collision.gameObject.SetActive(false);
            }
            //dougu.GetComponent<DouguManager>().Dougutool(waveDG);
            //collision.gameObject.SetActive(false);
            IconChild.SetActive(true);
           
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {

        if (collision.tag == "wall")
        {
            velocity = -0.05f;
        }
    }

    public void Choice()
    {
        int a = 0;
        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
            a++;
        }
        else if(Input.GetKeyDown(KeyCode.RightArrow))
        {
            a--;
        }
        
        if(Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("q");
            dougu.GetComponent<DouguManager>().ItemSkill(a);
        }
    }

    // Update is called once per frame
    void Update()
    {
        Choice();
        float x = 0;
        float y = 0;
        if (Input.GetKey(KeyCode.A))
        {
            x = -0.1f;
            //transform.position = new Vector3(transform.position.x - 1, transform.position.y);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            x = +0.1f;
            //transform.position = new Vector3(transform.position.x+1, transform.position.y);
        }
        if(Input.GetKeyDown(KeyCode.C))
        {
            y = 1.5f;
        }
        transform.position = new Vector3(transform.position.x + x, transform.position.y+y + velocity);
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, LeftWall.x, RightWall.x),
            Mathf.Clamp(transform.position.y, BottomWall.y, 180.5f), transform.position.z);
    }
}
