﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sheepscript : MonoBehaviour
{
    float y;
    float jump;
    int count;
    public int revcount;
    public float velocity;
    public Vector3 LeftWall;
    public Vector3 RightWall;
    public Vector3 BottomWall;
    // Start is called before the first frame update
    void Start()
    {
        count = 0;
        jump = 0;
        y -= 0.05f;
        LeftWall = GameObject.Find("leftwall").transform.position;
        RightWall = GameObject.Find("rightwall").transform.position;
        BottomWall = GameObject.Find("bottomwall").transform.position;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        
            y = -0.05f;
            
        
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log(collision.name);
        if (collision.name == "leftwall")
        {
            int a = Random.Range(1, 3);
            if(a==1)
            {
                revcount = 1;
            }
            else
            { revcount = 3; }
            y -= 0.05f;
        }
        else if (collision.name == "rightwall")
        {
            int a = Random.Range(1, 3);
            if (a == 1)
            {
                revcount = 2;
            }
            else
            { revcount = 4; }
            y -= 0.05f;
        }
        else if (collision.name == "bottomwall")
        {

            revcount = Random.Range(1, 5);
            y = 0;

        }
    }

    public void reverse()
    {   
        switch (revcount)
        {
            case 0: velocity = 0.0f;break;
            case 1: velocity = 0.1f; jump =0; break;
            case 2: velocity = -0.1f;jump = 0; break;
            case 3: jump = 0.1f; velocity = 0.1f; count++;
                Debug.Log(count);
                if(count>=60)
                {
                    revcount = Random.Range(1, 3);
                    count = 0;
                }
                break;
            case 4: jump = 0.1f; velocity = -0.1f; count++;
                if (count >= 60)
                {
                    revcount = Random.Range(1, 3);
                    count = 0;
                }
                break;
        }
       
    }

    // Update is called once per frame
    void Update()
    {  
        //Debug.Log(revcount);
        reverse();

        transform.position = new Vector3(transform.position.x + velocity, transform.position.y+jump + y);
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, LeftWall.x, RightWall.x),
           Mathf.Clamp(transform.position.y, BottomWall.y+0.5f, 180.5f), transform.position.z);
    }
}
