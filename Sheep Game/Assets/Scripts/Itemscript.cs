﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Itemscript : MonoBehaviour
{
    public enum  Itemform
    {
        trap,
        wave,
        meat,
    }
    private int Endurance;//耐久
    private Itemform items;
    private GameObject item;
    float velocity;
    void Start()
    {
        items = Itemform.trap;
        item = null;
        Endurance = 3;
        velocity = -0.05f;
    }

    public void EndEndurance(int a)
    {
        this.Endurance = a;
    }

    public Itemform itemform()
    {
        return items;
    }

    public void Effect()
    {
        switch(items)
        {
            case Itemform.trap:
                {
                                  
                }
                break;
            case Itemform.wave:
                {

                }
                break;
        }    
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "wall")
        {
            velocity = 0;          
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y + velocity);
        if(Endurance<=0)
        {
            gameObject.SetActive(false);
        }
    }
}
